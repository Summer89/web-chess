#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'bezobiuk'


import copy
import constant


def to_move_a_piece(piece_will_be_move, possible_move, position):
    new_coordinates = list(possible_move.keys())[constant.INDEX_0]
    old_last_moved_piece = position[constant.LAST_MOVED_PIECE]
    data_for_return = [(piece_will_be_move, copy.deepcopy(position[constant.BOARD][piece_will_be_move]))]
    data_for_pop = [new_coordinates]
    if possible_move[new_coordinates][constant.TAKE_A_PIECE]:
        data_for_return.append((possible_move[new_coordinates][constant.TAKE_A_PIECE],
                                copy.deepcopy(position[constant.BOARD].pop(possible_move[new_coordinates]
                                                                           [constant.TAKE_A_PIECE]))))
    if possible_move[new_coordinates][constant.DROP_A_PIECE]:
        position[constant.BOARD][possible_move[new_coordinates][constant.INSERT_A_PIECE]] = \
            position[constant.BOARD][possible_move[new_coordinates][constant.DROP_A_PIECE]]
        data_for_return.append((possible_move[new_coordinates][constant.DROP_A_PIECE],
                                copy.deepcopy(position[constant.BOARD].pop(possible_move[new_coordinates]
                                                                           [constant.DROP_A_PIECE]))))
        data_for_pop.append(possible_move[new_coordinates][constant.INSERT_A_PIECE])
        position[constant.BOARD][possible_move[new_coordinates][constant.INSERT_A_PIECE]][
            constant.PREVIOUS_COORDINATE] = possible_move[new_coordinates][constant.DROP_A_PIECE]
    if possible_move[new_coordinates][constant.THE_PAWN_QUEENS]:
        position[constant.BOARD][possible_move[new_coordinates][constant.INSERT_A_PIECE]] =\
            {constant.NAME_IMAGE: possible_move[new_coordinates][constant.THE_PAWN_QUEENS],
             constant.COLOR_PIECE: constant.PIECE[possible_move[new_coordinates][constant.THE_PAWN_QUEENS]]
             [constant.COLOR_PIECE],
             constant.SHORT_NAME: constant.PIECE[possible_move[new_coordinates][constant.THE_PAWN_QUEENS]]
             [constant.SHORT_NAME],
             constant.PREVIOUS_COORDINATE: piece_will_be_move}
        position[constant.BOARD].pop(piece_will_be_move)
    else:
        position[constant.BOARD][new_coordinates] = position[constant.BOARD].pop(piece_will_be_move)
    position[constant.LAST_MOVED_PIECE] = new_coordinates
    position[constant.BOARD][new_coordinates][constant.PREVIOUS_COORDINATE] = piece_will_be_move
    return data_for_return, data_for_pop, old_last_moved_piece


def return_move(data_for_return, data_for_pop, old_last_moved_piece, position):
    for data in data_for_pop:
        position[constant.BOARD].pop(data)
    position[constant.BOARD].update(data_for_return)
    position[constant.LAST_MOVED_PIECE] = old_last_moved_piece
