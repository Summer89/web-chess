#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'bezobiuk'

# Результатом виклику функції get_possible_moves являється словник, де ключом є координати клітинки, на яку переносимо
# фігуру, а даними являється словник, з чотирьох записів:
#   1. ключ: "take_a_piece"- дані: координати клітини з якої знімають фігуру,під час бою, або None, якщо хід без бою;
#   2. ключ: "drop_a_piece"- дані: координати клітини з якої знімають фігуру,якщо відбувається рокіровка або None,
#           якщо хід без данщї події;
#   3. ключ: "insert_a_piece"- дані: координати клітини на яку ставлять фігуру, якщо відбувається рокіровка, або пере-
#           творення пішака, або None, якщо хід без даних подій;
#   4. ключ: "the_pawn_queens"- дані: name_image фігури,якщо відбувається заміна пішака на останній лінії, інакше False.


import math
import chess_move
import constant


def inside(x, y):  # перевіряє виход координати за розмір дошки
    return (constant.SIZE_X >= x >= 1) and (constant.SIZE_Y >= y >= 1)


def literal_to_numbers(string_coordinates):  # перетворює координати із строкового виду в числовий
    return constant.LITERAL_NUMBER[string_coordinates[0]], int(string_coordinates[1])


def numbers_to_literal(coordinates):  # перетворює координати із числового виду в строковий
    return sorted(constant.LITERAL_NUMBER)[coordinates[0]-1] + str(coordinates[1])


def piece_on_square(coordinates, position):
    return position[constant.BOARD][coordinates][constant.COLOR_PIECE] if coordinates in position[constant.BOARD] \
        else None


def standard_iteration(x, y, color, position, possible_moves, operation):  # Проводить стандартні маніпуляції над
    delta_x, delta_y = operation                                           # координатами для більшості шахматних фігур
    while True:
        if inside(x + delta_x, y + delta_y):
            coordinates_new_square = numbers_to_literal((x + delta_x, y + delta_y))
            pieces_color_on_square = piece_on_square(coordinates_new_square, position)
            if not pieces_color_on_square:
                possible_moves.append({coordinates_new_square: {constant.TAKE_A_PIECE: None,  # стандартний хід фігури
                                                                constant.DROP_A_PIECE: None,
                                                                constant.INSERT_A_PIECE: None,
                                                                constant.THE_PAWN_QUEENS: False}})
                x, y = x + delta_x, y + delta_y
                continue
            elif pieces_color_on_square:
                if pieces_color_on_square != color:
                    possible_moves.append({coordinates_new_square: {constant.TAKE_A_PIECE: coordinates_new_square,
                                                                    constant.DROP_A_PIECE: None,  # хід фігури з боєм
                                                                    constant.INSERT_A_PIECE: None,
                                                                    constant.THE_PAWN_QUEENS: False}})
        break
    return possible_moves


def secondary_iteration(x, y, color, position, possible_moves, operation):  # Проводить стандартні маніпуляції над
    delta_x, delta_y = operation                                            # координатами для деяких шахматних фігур
    if inside(x + delta_x, y + delta_y):
        coordinates_new_square = numbers_to_literal((x + delta_x, y + delta_y))
        pieces_color_on_square = piece_on_square(coordinates_new_square, position)
        if not pieces_color_on_square:
            possible_moves.append({coordinates_new_square: {constant.TAKE_A_PIECE: None,  # стандартний хід фігури
                                                            constant.DROP_A_PIECE: None,
                                                            constant.INSERT_A_PIECE: None,
                                                            constant.THE_PAWN_QUEENS: False}})
        elif pieces_color_on_square and pieces_color_on_square != color:
            possible_moves.append({coordinates_new_square: {constant.TAKE_A_PIECE: coordinates_new_square,  # хід
                                                            constant.DROP_A_PIECE: None,                # фігури з боєм
                                                            constant.INSERT_A_PIECE: None,
                                                            constant.THE_PAWN_QUEENS: False}})
    return possible_moves


def get_possible_moves_common(coordinates, position, operations, function):  # обчислює координати можливих ходів фігур
    possible_moves = []                                               # по заданих алгоритмах
    x, y = literal_to_numbers(coordinates)
    color = position[constant.BOARD][coordinates][constant.COLOR_PIECE]
    for operation in operations:
        possible_moves = function(x, y, color, position, possible_moves, operation)
    return possible_moves


function_for_piece = {}


def add_to_function_to_register(constant_key):
    def decor(func):
        function_for_piece[constant_key] = func
        return func
    return decor


@add_to_function_to_register(constant.SHORT_NAME_PAWN)
def get_possible_moves_for_pawn(coordinates, position):  # обчислює координати можливих ходів пішки
    color = position[constant.BOARD][coordinates][constant.COLOR_PIECE]
    x, y = literal_to_numbers(coordinates)
    possible_moves = get_attacked_moves_for_pawn(coordinates, x, y, color, position)
    direction = constant.WHITE_DIRECTION if color == constant.COLOR_WHITE else constant.BLACK_DIRECTION
    delta_x, delta_y = (0, direction)
    if inside(x + delta_x, y + delta_y):
        coordinates_new_square = numbers_to_literal((x + delta_x, y + delta_y))
        pieces_color_on_square = piece_on_square(coordinates_new_square, position)
        if not pieces_color_on_square:
            if (y == 7 and color == constant.COLOR_WHITE) or (y == 2 and color == constant.COLOR_BLACK):
                possible_moves.append({coordinates_new_square: {constant.TAKE_A_PIECE: None,  # хід з перетворенням
                                                                constant.DROP_A_PIECE: None,  # пішака
                                                                constant.INSERT_A_PIECE: coordinates_new_square,
                                                                constant.THE_PAWN_QUEENS:
                                                                constant.NAME_IMAGE_FOR_THE_PAWN_QUINCE[color][0]}})
            else:
                possible_moves.append({coordinates_new_square: {constant.TAKE_A_PIECE: None,  # стандартний хід пішака
                                                                constant.DROP_A_PIECE: None,
                                                                constant.INSERT_A_PIECE: None,
                                                                constant.THE_PAWN_QUEENS: False}})
            y_compare = constant.WHITE_Y if color == constant.COLOR_WHITE else constant.BLACK_Y
            if y == y_compare:
                delta_x, delta_y = (0, 2 * direction)
                coordinates_new_square = numbers_to_literal((x + delta_x, y + delta_y))
                pieces_color_on_square = piece_on_square(coordinates_new_square, position)
                if not pieces_color_on_square:
                    possible_moves.append({coordinates_new_square: {constant.TAKE_A_PIECE: None,  # стандартний хід
                                                                    constant.DROP_A_PIECE: None,  # пішака
                                                                    constant.INSERT_A_PIECE: None,
                                                                    constant.THE_PAWN_QUEENS: False}})
    return possible_moves


def get_attacked_moves_for_pawn(coordinates, x, y, color, position):  # вираховує можливі ходи пішака з боєм
    possible_moves = []                                               # і з боєм на проході
    direction = constant.WHITE_DIRECTION if color == constant.COLOR_WHITE else constant.BLACK_DIRECTION
    operation = [(1, direction), (-1, direction)]
    for delta_x, delta_y in operation:
        if inside(x + delta_x, y + delta_y):
            coordinates_new_square = numbers_to_literal((x + delta_x, y + delta_y))
            pieces_color_on_square = piece_on_square(coordinates_new_square, position)
            if pieces_color_on_square and pieces_color_on_square != color:  # хід з перетворенням пішака і боєм
                if (y == 7 and color == constant.COLOR_WHITE) or (y == 2 and color == constant.COLOR_BLACK):
                    possible_moves.append({coordinates_new_square: {constant.TAKE_A_PIECE: coordinates_new_square,
                                                                    constant.DROP_A_PIECE: None,
                                                                    constant.INSERT_A_PIECE: coordinates_new_square,
                                                                    constant.THE_PAWN_QUEENS:
                                                                    constant.NAME_IMAGE_FOR_THE_PAWN_QUINCE[color][0]}})
                else:
                    possible_moves.append({coordinates_new_square: {constant.TAKE_A_PIECE: coordinates_new_square,
                                                                    constant.DROP_A_PIECE: None,  # хід пішака з боєм
                                                                    constant.INSERT_A_PIECE: None,
                                                                    constant.THE_PAWN_QUEENS: False}})
            y_compare = constant.WHITE_TUNNEL_Y if color == constant.COLOR_WHITE else constant.BLACK_TUNNEL_Y
            if y == y_compare:
                d_x, d_y = (delta_x, 2) if color == constant.COLOR_WHITE else (delta_x, -2)
                check_x, check_y = x + d_x, y
                if inside(check_x, check_y):
                    coordinates_check = numbers_to_literal((check_x, check_y))
                    color_for_compare = piece_on_square(coordinates_check, position)
                    if color_for_compare and color_for_compare != color:
                        last_moved_piece = position[constant.LAST_MOVED_PIECE]
                        coordinates_old_square = numbers_to_literal((x + d_x, y + d_y))
                        if last_moved_piece \
                                and position[constant.BOARD][last_moved_piece][constant.SHORT_NAME] == \
                                position[constant.BOARD][coordinates][constant.SHORT_NAME] and\
                                position[constant.BOARD][last_moved_piece][constant.PREVIOUS_COORDINATE] \
                                == coordinates_old_square and not pieces_color_on_square:  # хід пішака з боєм
                            possible_moves.append({coordinates_new_square: {constant.TAKE_A_PIECE: coordinates_check,
                                                                            constant.DROP_A_PIECE: None,  # на проході
                                                                            constant.INSERT_A_PIECE: None,
                                                                            constant.THE_PAWN_QUEENS: False}})
    return possible_moves


@add_to_function_to_register(constant.SHORT_NAME_KING)
def get_possible_moves_for_king(coordinates, position, terms=True):  # обчислює можливі ходи короля
    color = position[constant.BOARD][coordinates][constant.COLOR_PIECE]
    operations = [(-1, 0), (1, 0), (0, 1), (0, -1), (1, 1), (1, -1), (-1, -1), (-1, 1)]
    castling_operations = ((3, 2, 1), (-4, -2, -1))
    possible_moves = get_possible_moves_common(coordinates, position, operations, secondary_iteration)
    if not terms:
        return possible_moves
    x, y = literal_to_numbers(coordinates)
    if not square_is_attacked(coordinates, position, False, color):
        for delta_x, increment_x, delta_rook_x in castling_operations:
            notation_move = constant.SHORT_CASTLING if delta_x > 0 else constant.LONG_CASTLING
            if inside(x + delta_x, y):
                coordinate_for_check = numbers_to_literal((x + delta_x, y))
                if piece_on_square(coordinates, position) and\
                    not position[constant.BOARD][coordinates][constant.PREVIOUS_COORDINATE] and\
                    piece_on_square(coordinate_for_check, position) and\
                        not position[constant.BOARD][coordinate_for_check][constant.PREVIOUS_COORDINATE]:
                    indicator = True
                    for d_x in range(1, int(math.fabs(delta_x))):
                        if piece_on_square(numbers_to_literal(
                                (x + int(math.copysign(d_x, delta_x)), y)), position):  # математична хрінь щоб
                            indicator = False   # зберегти напрямок та знак даних для проведення перевірки
                    for d_x in range(1, int(math.fabs(increment_x))):
                        if square_is_attacked(numbers_to_literal((x + int(math.copysign
                                                                 (d_x, increment_x)), y)), position, False, color):
                            indicator = False
                    if indicator:
                        possible_moves.append({numbers_to_literal((x + increment_x, y)):
                                              {constant.TAKE_A_PIECE: None,  # рокіровка
                                               constant.DROP_A_PIECE: numbers_to_literal((x + delta_x, y)),
                                               constant.INSERT_A_PIECE: numbers_to_literal((x + delta_rook_x, y)),
                                               constant.THE_PAWN_QUEENS: False,
                                               constant.NOTATION: notation_move}})
    return possible_moves


@add_to_function_to_register(constant.SHORT_NAME_ROOK)
def get_possible_moves_for_rook(coordinates, position):  # обчислює можливі ходи тури
    operations = [(-1, 0), (1, 0), (0, 1), (0, -1)]
    possible_moves = get_possible_moves_common(coordinates, position, operations, standard_iteration)
    return possible_moves


@add_to_function_to_register(constant.SHORT_NAME_KNIGHT)
def get_possible_moves_for_knight(coordinates, position):  # обчислює можливі ходи коня
    operations = [(-1, 2), (1, 2), (2, 1), (2, -1), (1, -2), (-1, -2), (-2, -1), (-2, 1)]
    possible_moves = get_possible_moves_common(coordinates, position, operations, secondary_iteration)
    return possible_moves


@add_to_function_to_register(constant.SHORT_NAME_BISHOP)
def get_possible_moves_for_bishop(coordinates, position):  # обчислює можливі ходи слона
    operations = [(1, 1), (1, -1), (-1, -1), (-1, 1)]
    possible_moves = get_possible_moves_common(coordinates, position, operations, standard_iteration)
    return possible_moves


@add_to_function_to_register(constant.SHORT_NAME_QUEEN)
def get_possible_moves_for_queen(coordinates, position):  # обчислює можливі ходи королеви
    operations = [(-1, 0), (1, 0), (0, 1), (0, -1), (1, 1), (1, -1), (-1, -1), (-1, 1)]
    possible_moves = get_possible_moves_common(coordinates, position, operations, standard_iteration)
    return possible_moves


def return_function(short_name_piece):
    return function_for_piece[short_name_piece]


def move_notation(start, finish, move, name_piece, event):
    name = '' if name_piece == constant.SHORT_NAME_PAWN else name_piece
    characteristic_of_move = 'x' if move[finish][constant.TAKE_A_PIECE] else '-'
    the_pawn_quince = constant.PIECE[move[finish][constant.THE_PAWN_QUEENS]][constant.SHORT_NAME] if \
        move[finish][constant.THE_PAWN_QUEENS] else ''
    result_move = constant.STATUS_MOVE_NOTATION[event] if event else ''
    notation = name + start.lower() + characteristic_of_move + finish.lower() + the_pawn_quince + result_move
    return notation


def change_notation(old_notation, insert_elem, drop_elem, shift):
    return old_notation.replace(drop_elem, insert_elem) + shift


def return_status_move(position, terms, color):
    king_of_enemy = return_piece(constant.SHORT_NAME_KING, constant.COLOR_FLIP_FLOP[color], position)
    check_the_king = square_is_attacked(king_of_enemy, position, terms, constant.COLOR_FLIP_FLOP[color])
    status = constant.CHECKS[check_the_king]
    return status


def possible_moves_generator(position, terms, color):
    for coordinate_piece in position[constant.BOARD]:
        if position[constant.BOARD][coordinate_piece][constant.COLOR_PIECE] != color:
            short_name_piece = position[constant.BOARD][coordinate_piece][constant.SHORT_NAME]
            if short_name_piece == constant.SHORT_NAME_KING:
                yield return_function(short_name_piece)(coordinate_piece, position, terms)
            else:
                yield return_function(short_name_piece)(coordinate_piece, position)


def return_possible_moves(position, terms, color):
    all_possible_moves = possible_moves_generator(position, terms, color)
    return [i for i in all_possible_moves]


def how_many_pieces_attacking(all_moves, coordinates):
    return len([coordinates for moves in all_moves for move in moves for key in move if coordinates == key])


def square_is_attacked(coordinates, position, terms, color):  # перевіряє, чи клітинка під боєм
    all_moves = return_possible_moves(position, terms, color)
    how_many_pieces = how_many_pieces_attacking(all_moves, coordinates)
    return how_many_pieces


def get_possible_moves(coordinates, position):  # викликає правильну функцію, за координатами
    short_name_piece = position[constant.BOARD][coordinates][constant.SHORT_NAME]
    return return_function(short_name_piece)(coordinates, position)


def return_piece(short_name, color, position):  # повертає координати фігури з певним іменем та кольором
    for piece in position[constant.BOARD]:
        if position[constant.BOARD][piece][constant.SHORT_NAME] == short_name and\
           position[constant.BOARD][piece][constant.COLOR_PIECE] == color:
            return piece


def get_real_possible_moves(coordinates, position):  # повертає можливі ходи після перевірки короля на шах
    real_possible_moves = []
    color = position[constant.BOARD][coordinates][constant.COLOR_PIECE]
    piece = position[constant.BOARD][coordinates][constant.SHORT_NAME]
    for move in get_possible_moves(coordinates, position):
        data_for_return, data_for_pop, old_last_moved_piece = chess_move.to_move_a_piece(coordinates, move, position)
        king = return_piece(constant.SHORT_NAME_KING, color, position)
        check_my_king = square_is_attacked(king, position, False, color)
        if not check_my_king:
            status_move = return_status_move(position, False, color)
            for i in move:
                move[i][constant.STATUS_MOVE] = status_move
                if constant.NOTATION not in move[i]:
                    move[i][constant.NOTATION] = move_notation(coordinates, i, move, piece, status_move)
            real_possible_moves.append(move)
        chess_move.return_move(data_for_return, data_for_pop, old_last_moved_piece, position)
    return real_possible_moves


def evaluation_position(position, color):
    moves = [i for i in gen_real_pos_move(position, color) if i]
    return len(moves)


def gen_real_pos_move(position, color):
    for coordinate_piece in position[constant.BOARD]:
        if position[constant.BOARD][coordinate_piece][constant.COLOR_PIECE] == color:
            yield get_real_possible_moves(coordinate_piece, position)
