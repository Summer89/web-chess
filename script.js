(function(){
var black = 'b';
var urlForPiece = prefix+'/chess/staticfiles/images/';
var urlForLoadPage = prefix+'/chess/start_position/'+playerId;
var urlForPossibleMoves =  prefix+'/chess/highlight_possible_moves/'+playerId;
var urlForMove = prefix+'/chess/move/'+playerId;
var urlForGiveMeMove = prefix+'/chess/give_my_move/'+playerId;
var urlForAction = prefix+'/chess/action/'+playerId+'/';
var urlForEnemyMove = prefix+'/chess/enemy_name/'+playerId;
var urlForDrawAccept = prefix+'/chess/draw/'+playerId;
var blackColor = "#CDBE70";
var strongBlackColor = "#9C9C9C";
var whiteColor = "#FAFAD2";
var strongWhiteColor = "#CFCFCF";
var darkGreyColor = "#5F9EA0";
var greyColor = "#DCDCDC";
var nameAbsent = "Ім'я ще невідоме...";
var noneColor = "";
var keyData = 'data';
var keyId = 'id';
var keySurrender = 's';
var keyDraw = 'd';
var possibleMoves = 'possible_moves';
var takeAPiece = 'take_a_piece';
var dropAPiece = 'drop_a_piece';
var keyInsertPiece = 'insert_a_piece';
var thePawnQuince = 'the_pawn_queens';
var historyMove = 'history_move';
var start = 'start';
var finish = 'finish';
var massageName = 'massage';
var statusDraw = 'status_draw';
var preOneStatusMove = 'Вам поставили';
var preTwoStatusMove = 'Ви поставили';
var surrenderAt = 'Ви здалися супротивнику';
var seeSurrender = 'Ваш супротивник здався';
var drawProp = 'Пропонування нічиї відбудеться після Вашого ходу (за правилами).';
var drawChoice = 'Вам запропонували нічию, для згоди натисніть сюди ...';
var drawPropInd = 'Ви запропонували супротивнику нічию, він ввагається ...';
var drawAcceptInd = 'Ви прийняли пропозицію нічиї.';
var drawAgreeInd = 'Супротивник погодився на нічию';
var massageDraw = 'Нічия...';
var statusGame = 'status_move';
var stalemate = 'stalemate';
var mate = 'mate';
var check = 'check';
var doubleCheck = 'double_check';
var massageDecode = {'stalemate': 'пат', 'mate': 'мат', 'check':'шах', 'double_check': 'подвійний шах'};
var notation = 'notation';
getXmlHttp(urlForLoadPage, true, boardGenerator);
	function boardGenerator(dataForBoard) {
		var myName = dataForBoard['my_name'];
		var enemyName = dataForBoard['enemy_name'];
        var colorPieces = dataForBoard['color'];
        var startPosition = dataForBoard['position'];
        var whoMoves = dataForBoard['who_moves'];
        var statusPosition = dataForBoard[statusGame];
        var dataForThePawnQueens = dataForBoard['data_for_the_pawn_queens'];
		var numberSquares = ['1','2','3','4','5','6','7','8'];
		var literalSquares = ['A','B','C','D','E','F','G','H'];
		if (colorPieces == black){
			numberSquares = numberSquares.reverse();
			literalSquares = literalSquares.reverse();
		}
		var fullBoard = document.createElement('div');
		fullBoard.id = 'full_board';
		var board = document.createElement('div');
		board.className = 'board';
		board.id = 'boarder';
		var columnNumbers = document.createElement('div');
		columnNumbers.className = 'outside_numbers';
		columnNumbers.id = 'column_numbers';
		for (var i = numberSquares.length-1; i >= 0; i--){ // Генератор чисел зліва дошки
			var number = document.createElement('div');
			number.className = 'numbers';
			number.innerHTML = numberSquares[i];
			columnNumbers.appendChild(number);
		}
		fullBoard.appendChild(columnNumbers);
		for (var i = 0; i < literalSquares.length; i++){ // Генератор дошки
			var column = document.createElement('div');
			column.className = 'column';
			column.id = 'column'+literalSquares[i];
			for (var j = numberSquares.length-1; j >= 0; j--) {
				var square = document.createElement('div');
				if ((i+1)%2&(j+1)%2|!((i+1)%2||(j+1)%2)){
					square.className = 'black square droppable';
					} else {
					square.className = 'white square droppable';
					}
				square.id = literalSquares[i] + numberSquares[j];
				if (square.id in startPosition){
					var image = document.createElement('img');
					image.src = urlForPiece + startPosition[square.id]['name_image'];
					if (startPosition[square.id]['color_piece'] == colorPieces){
						image.className = 'draggable';
						if (colorPieces == whoMoves){
							image.addEventListener("mouseover", highlightPossibleMoves);
							image.addEventListener("mouseout", highlightPossibleMovesFree);
						}
					}
					square.appendChild(image);
				}
				column.appendChild(square);
			}
			board.appendChild(column);
		}
		fullBoard.appendChild(board);
		highlightHref();
		actionConstruct('surrender', surrenderAtDiscretion);
		actionConstruct('draw', drawProposition);
		if(statusPosition){
			var preStatusMove = (colorPieces == whoMoves) ? preOneStatusMove : preTwoStatusMove;
			inscription(massageName, preStatusMove + ' ' + massageDecode[statusPosition]);
		}
		var rowLiterals = document.createElement('div');
		rowLiterals.className = 'outside_literals';
		rowLiterals.id = 'row_literals';
		for (var i = 0; i < literalSquares.length; i++){ // Генератор літер під дошкою
			var literal = document.createElement('div');
			literal.className = 'literals';
			literal.innerHTML = literalSquares[i];
			rowLiterals.appendChild(literal);
		}
		fullBoard.appendChild(rowLiterals);
		document.body.appendChild(fullBoard);
		var enemyName = (enemyName) ? enemyName : nameAbsent;
		inscription('my_name', myName);
		inscription('enemy_name', enemyName);
		if (colorPieces == whoMoves) {
			waitIndicatorOff();
		}
		waitIndicatorOff();
		loadHistory(dataForBoard[historyMove]);
		modalWindow(dataForThePawnQueens);
		forMoveElement();
		if (colorPieces != whoMoves){
			waitIndicatorOn();
			forStopElement();
			actionDestruct('surrender', surrenderAtDiscretion);
			actionDestruct('draw', drawProposition);
			enemyInGame();
		}
	}

	function enemyInGame(){
		var enemyName = document.getElementById("enemy_name").innerHTML;
		if (enemyName == nameAbsent){
				getXmlHttp(urlForEnemyMove, true, addedEnemy, 5000);
			}else{
				getXmlHttp(urlForGiveMeMove, true, checkMassage);
			}
	}

	function drawAcception(){
		inscription('draw_prop', drawAcceptInd);
		inscription('massage', massageDraw);
		var elem = document.getElementById("draw_prop");
		highlightElemOff(elem);
		elem.style.color = noneColor;
		stopGame();
		getXmlHttp(urlForDrawAccept, true, null, 250);
	}

	function addedEnemy(data){
		var enemyName = data['enemy_name'];
		inscription('enemy_name', enemyName);
		getXmlHttp(urlForGiveMeMove, true, checkMassage);
	}

	function surrenderAtDiscretion(){
		inscription(massageName, surrenderAt);
		var elem = document.getElementById("surrender");
		elem.style.color = noneColor;
		stopGame();
		getXmlHttp(urlForAction + 'surrender', true, null, 250);
	}

	function drawProposition(){
		inscription('draw_prop', drawProp);
		var elem = document.getElementById("draw");
		elem.style.color = noneColor;
		stopGame();
		getXmlHttp(urlForAction + 'draw', true, afterDrawProposition, 250);
	}

	function actionConstruct(idElem, func){
		var elem = document.getElementById(idElem);
		highlightElemOn(elem);
		elem.addEventListener("click", func);
		elem.classList.add('cursor');
		elem.classList.add('choice_action');
	}

	function actionDestruct(idElem, func){
		var elem = document.getElementById(idElem);
		highlightElemOff(elem);
		elem.removeEventListener("click", func);
		elem.classList.remove('cursor');
		elem.classList.remove('choice_action');
	}

	function drawMassages(){
		var elem = document.getElementById('draw_prop');
		var value = elem.innerHTML;
		if (value){
			if(value == drawChoice){
				actionDestruct('draw_prop', drawAcception);
				elem.innerHTML = '';
			}else if(value == drawProp){
				elem.innerHTML = drawPropInd;
			}else if(value == drawPropInd){
				elem.innerHTML = '';
			}else if(value == drawAcceptInd){
				actionDestruct('draw_prop', drawAcception);
			}
		}
	}

	function mouseOnHref(elem, condition){
 		elem.style.color = condition ? "#000000" : noneColor;
 	}

	function highlightHref(){
 		var onmouseover = function() {mouseOnHref(this, true)};
    	var onmouseout = function() {mouseOnHref(this, false)};
    	var highlightingHrefs = document.getElementsByClassName("action");
    	for (var i=0; i<highlightingHrefs.length; i++){
    		highlightingHrefs[i].onmouseover = onmouseover;
    		highlightingHrefs[i].onmouseout = onmouseout;
    	}

	}

	function highlightElemOn(elem){
		elem.onmouseover = function() {this.style.color = "#000000";};
		elem.onmouseout = function() {this.style.color = noneColor;};
	}

	function highlightElemOff(elem){
		elem.onmouseover = function() {return false};
		elem.onmouseout = function() {return false};
	}

	function loadHistory(historyData){
		if (historyData){
			var history = document.getElementById("history");
			for (var i = 0; i < historyData.length; i++){
				var para = document.createElement("div");
				para.classList.add('moves_notation');
				var number = document.createElement("div");
				number.classList.add('div_notation');
				number.classList.add('div_num');
				number.appendChild(document.createTextNode(String(i+1) + '.'));
				para.appendChild(number);
				var firstMove = document.createElement("div");
				firstMove.classList.add('div_notation');
				firstMove.classList.add('div_move');
				firstMove.classList.add('first_notation');
				firstMove.appendChild(document.createTextNode(historyData[i][0]));
				para.appendChild(firstMove);
				if (historyData[i].length > 1){
					var secondMove = document.createElement("div");
					secondMove.classList.add('div_notation');
					secondMove.classList.add('div_move');
					secondMove.classList.add('second_notation');
					secondMove.appendChild(document.createTextNode(historyData[i][1]));
					para.appendChild(secondMove);
				}
				history.appendChild(para);
			}
			history.scrollTop = history.scrollHeight;
		}
	}

	function updateHistory(reportData){
		var history = document.getElementById("history");
		var counter = history.getElementsByClassName('div_num').length;
		var last_notation = history.getElementsByClassName('second_notation').length;
		var para = history.lastChild;
		if (!para || (para && (last_notation == counter))){
			var para = document.createElement("div");
			para.classList.add('moves_notation');
			var number = document.createElement("div");
			number.classList.add('div_notation');
			number.classList.add('div_num');
			number.appendChild(document.createTextNode(String(counter+1) + '.'));
			para.appendChild(number);
			var firstMove = document.createElement("div");
			firstMove.classList.add('div_notation');
			firstMove.classList.add('div_move');
			firstMove.classList.add('first_notation');
			firstMove.appendChild(document.createTextNode(reportData));
			para.appendChild(firstMove);
			history.appendChild(para);
		}else{
			var secondMove = document.createElement("div");
				secondMove.classList.add('div_notation');
				secondMove.classList.add('div_move');
				secondMove.classList.add('second_notation');
				secondMove.appendChild(document.createTextNode(reportData));
				para.appendChild(secondMove);
		}
		history.scrollTop = history.scrollHeight;
	}

	function modalWindow(dataForThePawnQueens) { //Модальне вікно вибору фігури для ходу з підстановкою
    	var parent = document.getElementsByTagName('body')[0];
    	var obj = parent.firstChild;
    	block = document.createElement('div');
    	block.id = 'blockscreen';
    	parent.insertBefore(block, obj);
    	block.style.display = 'none';
    	var parent = document.getElementsByTagName('body')[0];
    	var obj = parent.firstChild;
    	win = document.createElement('div');
    	win.id = 'modalwindow';
    	win.style.padding = '0 0 0 0';
    	parent.insertBefore(win, obj);
    	var onmouseover = function() {highlightSquare(this, true, darkGreyColor)};
    	var onmouseout = function() {highlightSquare(this, false, darkGreyColor)};
    	for (var i = 0; i < dataForThePawnQueens.length; i++) {
    	   	var square = document.createElement('div');
    	   	square.onmouseover = onmouseover;
    	   	square.onmouseout = onmouseout;
    	   	square.className = 'square white column';
    	   	var image = document.createElement('img');
			image.src = urlForPiece + dataForThePawnQueens[i];
			image.className = 'image_for_the_pawn_quince';
			image.name = dataForThePawnQueens[i];
			square.appendChild(image);
			win.appendChild(square);

    	}
    	win.style.display = 'none';
    	win.style.left = '43%';
    	win.style.top = '42%';
    }

 	function close() {
    	document.getElementById('blockscreen').style.display = 'none';
    	document.getElementById('modalwindow').style.display = 'none';
    	removeClickInModalWindow();
    }
   	function show() {
   		document.getElementById('blockscreen').style.display = 'inline';
    	document.getElementById('modalwindow').style.display = 'inline';
    }

 	function highlightSquare(square, condition, color){
 		square.style.background = condition ? color : noneColor;
 	}

 	function addClickInModalWindow(dataForUrl, data, condition){
 		var imageForThePawnQuince = document.getElementsByClassName('image_for_the_pawn_quince');
		for (var i = 0; i < imageForThePawnQuince.length; i++){
			imageForThePawnQuince[i].onclick = function() {close(); thePawnQuinceMove(this.name,
																						dataForUrl, data, condition)};
		}
 	}

 	function removeClickInModalWindow(){
 		var imageForThePawnQuince = document.getElementsByClassName('image_for_the_pawn_quince');
		for (var i = 0; i < imageForThePawnQuince.length; i++){
			imageForThePawnQuince[i].onclick = null;
		}
 	}

	var scrollElement = document.getElementById("history");
	scrollElement.classList.add('scroll_no');
	scrollElement.addEventListener("mouseover", function(){scrollOn(this);});
	scrollElement.addEventListener("mouseout", function(){scrollOut(this);});

	function scrollOn(elem){
		elem.classList.remove('scroll_no');
		elem.classList.add('scroll_yes');
	}
	function scrollOut(elem){
		elem.classList.remove('scroll_yes');
		elem.classList.add('scroll_no');
	}

	var lastMoveIndication = [];
	var highlightingElem = [];
	var dataForPossibleMoves;

	function highlightPossibleMoves(event){ // дає AJAX запит на можливі ходи фігури
		var elem = event.currentTarget;
    	var highlightElem = elem.closest('div');
		getXmlHttp(urlForPossibleMoves + '/' + highlightElem.id, false, possibleMovesDataProcessing, 250);
	}

    function possibleMovesDataProcessing(data){ // отримує дані можливих ходів та обробляє їх
    	if (data[possibleMoves]){
    		dataForPossibleMoves = data[possibleMoves]
    	}
    	for (var i = 0; i < data[keyData].length; i++){
    		var elemForHighlighting = document.getElementById(data[keyData][i]);
    		elemForHighlighting.classList.add('highlight');
    		highlightingElem.push(elemForHighlighting);
    	}
    }

    function highlightPossibleMovesFree(){ // знімає підсвітку з клітинок можливих ходів
    	if (!statusMove){
    		for (var i = 0; i < highlightingElem.length; i++){
    			var elemForHighlightingFree = highlightingElem[i];
    			elemForHighlightingFree.classList.remove('highlight');
    		}
    		highlightingElem = [];
    	}
    }

    document.ondragstart = function() { return false };
    document.body.onselectstart = function() { return false };

	function forMoveElement(){ //для можливості руху фігур
		document.addEventListener("mousedown", takeElement);
    	document.addEventListener("mouseup", endDragElement);
    	document.addEventListener("mousemove", startDragElement);
    }

    function forStopElement(){ //для обездвиження фігур
    	document.removeEventListener("mousedown", takeElement);
    	document.removeEventListener("mouseup", endDragElement);
    	document.removeEventListener("mousemove", startDragElement);
    }

    var currentDropTarget;
    var dragObject = {};
    var startMove;
    var statusMove;

    function takeElement(event) {
    	if (event.which != 1) {
    		return;
  		}
  		var elem = event.target.closest('.draggable');
  		if(elem){
  			startMove = elem.closest('div');
  		}
  			if (!elem) return;
  		dragObject.elem = elem;
  		dragObject.downX = event.pageX;
  		dragObject.downY = event.pageY;
	}

	function startDragElement(event) {
  		if (!dragObject.elem) {return;}
  		if ( !dragObject.avatar ) {
    		var moveX = event.pageX - dragObject.downX;
    		var moveY = event.pageY - dragObject.downY;
    		if ( Math.abs(moveX) < 3 && Math.abs(moveY) < 3 ) {
    			return;
    		}
			dragObject.avatar = createAvatar(event);
    		if (!dragObject.avatar) {
      			dragObject = {};
      			return;
    		}
    		var coords = getCoords(dragObject.avatar);
    		dragObject.shiftX = dragObject.downX - coords.left;
    		dragObject.shiftY = dragObject.downY - coords.top;
			startDrag(event);
  		}
  		if(lastMoveIndication){
  			for (var i = 0; i < lastMoveIndication.length; i++){
  				highlightSquare(lastMoveIndication[i], false, strongWhiteColor);
  			}
  			lastMoveIndication = [];
  		}
  		dragObject.avatar.style.left = event.pageX - dragObject.shiftX + 'px';
  		dragObject.avatar.style.top = event.pageY - dragObject.shiftY + 'px';
  		highlightElement(event);
  		changeDraggableClass();
  		statusMove = true;
  		return false;
	}

	function highlightElement(event){
		var newTarget = findDroppable(event);
        if (currentDropTarget != newTarget) {
            if (currentDropTarget) {
            	highlightSquare(currentDropTarget, false, darkGreyColor);
            }
            if (newTarget) {
            	highlightSquare(newTarget, true, darkGreyColor);
            }
            currentDropTarget = newTarget;
        }
	}

	function createAvatar(event) {
  		var avatar = dragObject.elem;
  		var old = {
    	parent: avatar.parentNode,
    	nextSibling: avatar.nextSibling,
    	position: avatar.position || '',
    	left: avatar.left || '',
    	top: avatar.top || '',
    	zIndex: avatar.zIndex || ''
  		};
  		avatar.rollback = function() {
    	old.parent.insertBefore(avatar, old.nextSibling);
    	avatar.style.position = old.position;
    	avatar.style.left = old.left;
    	avatar.style.top = old.top;
    	avatar.style.zIndex = old.zIndex;
    	dragObject = {};
    	statusMove = false;
    	highlightPossibleMovesFree();
    	returnDraggableClass();
  		};
		return avatar;
	}

	function startDrag(event) {
  		var avatar = dragObject.avatar;
  		document.body.appendChild(avatar);
  		avatar.style.zIndex = 9999;
  		avatar.style.position = 'absolute';
	}

	function endDrag(event) {
  		var avatar = dragObject.avatar;
  		avatar.style.zIndex = '';
  		avatar.style.position = '';
	}

	function getCoords(elem) {
  		var box = elem.getBoundingClientRect();
  		var body = document.body;
  		var docEl = document.documentElement;
  		var scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;
  		var scrollLeft = window.pageXOffset || docEl.scrollLeft || body.scrollLeft;
  		var clientTop = docEl.clientTop || body.clientTop || 0;
  		var clientLeft = docEl.clientLeft || body.clientLeft || 0;
  		var top = box.top + scrollTop - clientTop;
  		var left = box.left + scrollLeft - clientLeft;
  		return {
    			top: top,
    			left: left
  				};
	}

	function getDataByKey(arr, key){
		var dataElement = [];
		for (var i = 0; i < arr.length; i++){
			dataElement.push(arr[i][key]);
		}
		return dataElement;
	}

	function endDragElement(event) {
		var condition = false;
		if (dragObject.avatar){
  			var dropElem = findDroppable(event);
  			var listId = getDataByKey(highlightingElem, keyId);
  			if (dropElem){
  				for (var i = 0; i < listId.length; i++){
  					if (dropElem.id == listId[i]){
  						condition = true;
  					}
  				}
  			}
  			if (dropElem && condition){
  				var avatar = dragObject.avatar;
  				endDrag(event);
  				if(dropElem.childNodes.length != 0){
  					dropElem.removeChild(dropElem.firstChild);
  				}
  				dropElem.appendChild(dragObject.avatar);

  			} else {
    			dragObject.avatar.rollback();
  			}
  			if (currentDropTarget){
  				highlightSquare(currentDropTarget, false, darkGreyColor);
  			}
  			statusMove = false;
  			returnDraggableClass();
  			highlightPossibleMovesFree();
		}
		dragObject = {};
		if (dropElem && (startMove.id != dropElem.id) && condition){
			for (var i = 0; i < dataForPossibleMoves.length; i++){
  				if (dropElem.id == dataForPossibleMoves[i][finish]){
  					var tunnelData = urlForMove + '/' + startMove.id + '/' + dropElem.id + '/';
  					makeMove(dataForPossibleMoves[i], tunnelData, true);
  				}
  			}
		}
		condition = false;
	}

	function findDroppable(event) {
  		dragObject.avatar.hidden = true;
  		var elem = document.elementFromPoint(event.clientX, event.clientY);
  		dragObject.avatar.hidden = false;
  		if (elem == null) {
    		return null;
  		}
  		return elem.closest('.droppable');
	}

	function waitYourMove(data){
		waitIndicatorOn();
		forStopElement();
		actionDestruct('surrender', surrenderAtDiscretion);
		actionDestruct('draw', drawProposition);
		enemyInGame();
	}

	function thePawnQuinceMove(nameImage, dataForUrl, data, condition){ // хід з заміною пішака
		var insertAPiece = document.getElementById(data[keyInsertPiece]);
		var pieceForDrop = insertAPiece.firstChild;
		insertAPiece.removeChild(pieceForDrop);
		var image = document.createElement('img');
		image.src = urlForPiece + nameImage;
		if (condition){
			image.className = 'draggable';
		}
		insertAPiece.appendChild(image);
		if (condition){
			showCallback(nameImage, dataForUrl, data);
		}else{
				iAmPlay(data);
			}
	}

	function checkMassage(data){
		waitIndicatorOff();
		if((!data[statusGame] || (data[statusGame] != keySurrender)) &&
		(!data[statusGame] || (data[statusGame] != keyDraw))){
			makeMove(data);
		}else{
			var valueMassage;
			if(data[statusGame] == keySurrender){
				valueMassage = seeSurrender;
			}else{
				valueMassage = massageDraw;
				inscription('draw_prop', drawAgreeInd);
			}
			inscription(massageName, valueMassage);
			stopGame();
		}
	}

	function makeMove(data, tunnelData, condition){  // власне робить хід фігури, condition на необхідність визову
		tunnelData = tunnelData || false;  // модального вікна ходу пішака з заміною (тобто коли ти ходиш true, коли
		condition = condition || false;  // супротивник false)
		runGame();
		drawMassages();
		var startSquare = document.getElementById(data[start]);
		var endSquare = document.getElementById(data[finish]);
		if (!condition){
			highlightSquare(startSquare, true, strongBlackColor);
			highlightSquare(endSquare, true, strongWhiteColor);
			lastMoveIndication.push(startSquare);
			lastMoveIndication.push(endSquare);
		}
		if (data[takeAPiece] && !condition){
			var takingPiece = document.getElementById(data[takeAPiece]);
			var pieceForRemove = takingPiece.firstChild;
			if (pieceForRemove){
				takingPiece.removeChild(pieceForRemove);
			}
		}
		if (data[takeAPiece] && (data[finish] != data[takeAPiece])){
			var takingPiece = document.getElementById(data[takeAPiece]);
			var pieceForRemove = takingPiece.firstChild;
			if (pieceForRemove){
				takingPiece.removeChild(pieceForRemove);
			}
		}
		if (data[dropAPiece]){
			var droppingPiece = document.getElementById(data[dropAPiece]);
			var pieceForDrop = droppingPiece.firstChild;
			droppingPiece.removeChild(pieceForDrop);
			var insertAPiece = document.getElementById(data[keyInsertPiece]);
			insertAPiece.appendChild(pieceForDrop);
		}
		var pieceForMove = startSquare.firstChild;
		if (pieceForMove){
			startSquare.removeChild(pieceForMove);
			endSquare.appendChild(pieceForMove);
		}
		if (data[thePawnQuince]){
			if (condition){
				addClickInModalWindow(tunnelData, data, condition);
				show();
			}
			else if (!condition){
				if(data['status_draw'] && data['status_draw'] == keyDraw){
					inscription('draw_prop', drawChoice);
					actionConstruct('draw_prop', drawAcception);
				}
				if(data[statusGame]){
					inscription(massageName, preOneStatusMove + ' ' + massageDecode[data[statusGame]]);
					if (data[statusGame]==mate || data[statusGame]==stalemate){
						stopGame();
						}
				}else{
					inscription(massageName, '');
				}
				thePawnQuinceMove(data[thePawnQuince], tunnelData, data, condition);
			}
		}
		if (!data[thePawnQuince]){
			if (condition){
				showCallback(data[thePawnQuince], tunnelData, data);
			}
			else if (!condition){
				if(data['status_draw'] && data['status_draw'] == keyDraw){
					inscription('draw_prop', drawChoice);
					actionConstruct('draw_prop', drawAcception);
				}
					if(data[statusGame]){
						inscription(massageName, preOneStatusMove + ' ' + massageDecode[data[statusGame]]);
						if (data[statusGame]==mate || data[statusGame]==stalemate){
							stopGame();
						}
						iAmPlay(data);
					}else{
						inscription(massageName, '');
						iAmPlay(data);
					}
			}
		}
	}

	function iAmPlay(data){
		updateHistory(data[notation]);
		dataForPossibleMoves = [];
		returnDraggableClass();
		forMoveElement();
	}

	function showCallback(returnData, dataForUrl, data) {
		dataForPossibleMoves = [];
		stopGame();
		getXmlHttp(dataForUrl + returnData, true, checkStatus);
	}

	function checkStatus(data){
		updateHistory(data[notation]);
		if(data[statusGame]){
			inscription(massageName, preTwoStatusMove + ' ' + massageDecode[data[statusGame]]);
		}else{
			inscription(massageName, '');
		}
		if (data[statusGame]==mate || data[statusGame]==stalemate){
			stopGame();
		}else{
			waitYourMove(data);
		}
	}

	function stopGame(){
		changeDraggableClass();
		forStopElement();
		actionDestruct('surrender', surrenderAtDiscretion);
		actionDestruct('draw', drawProposition);
		actionDestruct('draw_prop', drawAcception);
	}

	function runGame(){
		returnDraggableClass();
		forMoveElement();
		actionConstruct('surrender', surrenderAtDiscretion);
		actionConstruct('draw', drawProposition);
	}

	function afterDrawProposition(){
		runGame();
		actionDestruct('draw', drawProposition);
	}

	function inscription(idElem, dataForMassage){
		var massage = document.getElementById(idElem);
		massage.innerHTML = dataForMassage;
	}

	var myInterval = null;

	function getXmlHttp(url, status, func, speed){
		speed = speed || 1500;
  		var xmlhttp;
  		try {
    		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
 		} catch (e) {
    		try {
      			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    		} catch (E) {
      			xmlhttp = false;
    		}
  		}
  		try {
  			if (! xmlhttp && typeof XMLHttpRequest != 'undefined') {
    			xmlhttp = new XMLHttpRequest();
    		}
    	} catch (e) {
    		alert('Ваш броузер не підтримує AJAX!');
    		xmlhttp = false;
  		}
		if (xmlhttp){
			xmlhttp.open("GET", url, status);
			xmlhttp.onreadystatechange = function () {
				if (myInterval == null){
					myInterval = setInterval(getXmlHttp, speed, url, status, func);
				}
				if (xmlhttp.readyState == 4){
		    		if (xmlhttp.status == 200) {
		        		data = JSON.parse(xmlhttp.responseText);
		        		if(myInterval && data['status']){
				    		clearInterval(myInterval);
				    		myInterval=null;
				    		if (func){
				    			func(data);
				    		}
						}
					}
				}
			};
			xmlhttp.send(null);
		}
	}

	function changeDraggableClass(){ // щоб не ставало кілька фігур на одну клітинку
		var draggableElement = document.getElementsByClassName('draggable');
		for (var i = 0; i < draggableElement.length; i++){
			draggableElement[i].removeEventListener("mouseover", highlightPossibleMoves);
			draggableElement[i].removeEventListener("mouseout", highlightPossibleMovesFree);
		}
	}

	function returnDraggableClass(){ // щоб не ставало кілька фігур на одну клітинку
		var draggableElement = document.getElementsByClassName('draggable');
		for (var i = 0; i < draggableElement.length; i++){
			draggableElement[i].addEventListener("mouseover", highlightPossibleMoves);
			draggableElement[i].addEventListener("mouseout", highlightPossibleMovesFree);
		}
	}

	function showAnime(idElem){
		var elem = document.getElementById(idElem);
		elem.classList.remove('hide');
		elem.classList.add('show');
	}

	function hideAnime(idElem){
		var elem = document.getElementById(idElem);
		elem.classList.remove('show');
		elem.classList.add('hide');
	}

	function waitIndicatorOn(){
		showAnime("anime");
	}

	function waitIndicatorOff(){
		hideAnime("anime");
	}

}) (window);