#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'bezobiuk'

import postgresql
import json
from uuid import uuid4 as uuid
from bottle import *
from beaker.middleware import SessionMiddleware
from time import time as second, ctime as time
from itertools import chain
from settings import session_opts, DB_CONNECTION_INFO, PREFIX_TO_URL
import constant
import chess_possible_moves
import chess_move

_app = Bottle()
app = application = SessionMiddleware(_app, session_opts)


def session_request():
    return request.environ.get('beaker.session')


db_connection = None


def get_db_connection():
    global db_connection
    if not db_connection:
        db_connection = postgresql.open(DB_CONNECTION_INFO)
    return db_connection


def execute_statement_fabric(statement_str):
    stat = None

    def prepare_statement():
        nonlocal stat
        if not stat:
            stat = get_db_connection().prepare(statement_str)
        return stat

    def execute_statement(*args):
        return prepare_statement()(*args)

    return execute_statement


def accept_invite_games(uuid_invite, name):
    session = session_request()
    session[constant.LIST_CHALLENGE] = session.get(constant.LIST_CHALLENGE, [])
    challenge_id = execute_statement_fabric(constant.SELECT_CHALLENGE_INVITE_STATEMENT)(uuid_invite)[0][0]
    color = constant.COLOR_FLIP_FLOP[execute_statement_fabric(constant.SELECT_COLOR_ENEMY_PLAYER)(challenge_id)[0][0]]
    if session[constant.LIST_CHALLENGE]:
        for player in session[constant.LIST_CHALLENGE]:
            test_challenge_id, col_player, my_name = execute_statement_fabric(constant.SELECT_CHALLENGE_STATEMENT
                                                                              )(player)[0]
            if test_challenge_id == challenge_id:
                return None
    uuid_player = create_player(color, name, challenge_id)
    accept_invite_time = time(second())
    execute_statement_fabric(constant.ACCEPT_INVITE_STATEMENT)(accept_invite_time, challenge_id)
    session[constant.LIST_CHALLENGE].append(uuid_player)
    session.save()
    return uuid_player


def generate_invite_games(color, name):
    challenge_id = execute_statement_fabric(constant.GET_SEQUENCE_STATEMENT)()[0][0]
    execute_statement_fabric(constant.INSERT_CHALLENGE_STATEMENT)(challenge_id)
    one_uuid, two_uuid = uuid(), uuid()
    execute_statement_fabric(constant.INSERT_INVITE_STATEMENT)(one_uuid, two_uuid, challenge_id)
    uuid_player = create_player(color, name, challenge_id)
    position = json.dumps(constant.START_POSITION)
    history = json.dumps([])
    last_move = json.dumps('')
    execute_statement_fabric(constant.CREATE_GAME_STATEMENT)(challenge_id, position, constant.COLOR_WHITE, history,
                                                             last_move, constant.COLOR_WHITE)
    session = session_request()
    session[constant.LIST_CHALLENGE] = session.get(constant.LIST_CHALLENGE, [])
    session[constant.LIST_CHALLENGE].append(uuid_player)
    session.save()
    return uuid_player, two_uuid


def create_player(color_piece, name_player, challenge_id):
    uuid_player = uuid()
    created_time = time(second())
    execute_statement_fabric(constant.CREATE_PLAYER_STATEMENT)(name_player, created_time, color_piece, challenge_id,
                                                               uuid_player)
    return uuid_player


def history_updater(history, notation):
    condition = len(history)
    if condition > 0 and len(history[condition-1]) < 2:
        history[condition-1].append(notation)
    else:
        history.append([notation])
    return history


def safe_route(func):
    # функція обробляє помилки.
    def new_func(*_args, **_kwargs):
        try:
            return func(*_args, **_kwargs)
        except Exception as e:
            print(func.__name__, e)
        return template(constant.INVALID_UUID_PAGE, prefix=PREFIX_TO_URL)
    return new_func


@_app.route(PREFIX_TO_URL+'/chess/staticfiles/<filepath:path>')  # підключає картинки, css і JavaScript
def server_static(filepath):
    return static_file(filepath, root='./')


@_app.route(PREFIX_TO_URL+'/web_chess/')  # початкова сторінка сервісу
@safe_route
def start_page_function():
    session = session_request()
    session[constant.LIST_CHALLENGE] = session.get(constant.LIST_CHALLENGE, [])  # uuid'и гравців, активних ігр
    active_games = []
    if session[constant.LIST_CHALLENGE]:
        for player in session[constant.LIST_CHALLENGE]:
            challenge, color, my_name = execute_statement_fabric(constant.SELECT_CHALLENGE_STATEMENT)(player)[0]
            who_moves = execute_statement_fabric(constant.SELECT_WHO_MOVES_STATEMENT)(challenge)[0][0]
            if who_moves != constant.E and who_moves != constant.SURRENDER and who_moves != constant.DRAW:
                color = constant.COLOR_FLIP_FLOP[color]
                acception = execute_statement_fabric(constant.SELECT_ACTIVE_GAMES_STATEMENT)(challenge, color)
                if acception:
                    name_player, start_game = acception[0]
                    active_games.append((str(name_player) + ' ' + str(start_game),
                                         constant.URL_CHESS_GAME + str(player)))
                else:
                    active_games.append((constant.NO_ACCEPTED_GAME, constant.URL_LINK_RIVAL + str(player)))
            else:
                session[constant.LIST_CHALLENGE].pop(session[constant.LIST_CHALLENGE].index(player))
                if not session[constant.LIST_CHALLENGE]:
                    active_games.append((constant.NO_ACTIVE_GAMES, None))
    else:
        active_games.append((constant.NO_ACTIVE_GAMES, None))
    session.save()
    return template(constant.START_PAGE, activeGames=json.dumps(active_games), prefix=PREFIX_TO_URL)


@_app.post(PREFIX_TO_URL+'/chess/')  # сторінка вибору дії
@safe_route
def selection_action():
    generate = request.forms.getunicode("generate")
    two_uuid = request.forms.getunicode("two_uuid").strip()
    accept = request.forms.getunicode("accept")
    color = request.forms.getunicode("color")
    name_player = request.forms.getunicode("name_player")
    uuid_your_invite = None
    page = None
    me_uuid = None
    if accept:
        me_uuid = accept_invite_games(two_uuid, name_player)
        page = constant.PAGE if me_uuid else constant.IMPOSSIBLE_PAGE
    elif generate:
        me_uuid, uuid_your_invite = generate_invite_games(color, name_player)
        page = constant.INVITE_PAGE
    return template(page, meUuid=me_uuid, invite=uuid_your_invite, prefix=PREFIX_TO_URL)


@_app.route(PREFIX_TO_URL+'/chess/link/<my_uuid>')  # якщо забув відіслати запрошення
@safe_route
def empty(my_uuid):
    challenge_id = execute_statement_fabric(constant.SELECT_CHALLENGE_STATEMENT)(my_uuid)[0][0]
    invite = execute_statement_fabric(constant.SELECT_INVITE_STATEMENT)(challenge_id)[0][0]
    return template(constant.INVITE_PAGE, meUuid=my_uuid, invite=invite, prefix=PREFIX_TO_URL)


@_app.route(PREFIX_TO_URL+'/chess/game/<my_uuid>')  # заходимо в партію
@safe_route
def game(my_uuid):
    return template(constant.PAGE, meUuid=my_uuid, prefix=PREFIX_TO_URL)


@_app.route(PREFIX_TO_URL+'/chess/start_position/<my_uuid>')  # AJAX запит, щоб розставити фігури на дошці
@safe_route
def start_position(my_uuid):
    challenge_id, color, my_name = execute_statement_fabric(constant.SELECT_CHALLENGE_STATEMENT)(my_uuid)[0]
    enemy_name = execute_statement_fabric(constant.SELECT_ENEMY_NAME_STATEMENT)(challenge_id,
                                                                                constant.COLOR_FLIP_FLOP[color])
    enemy_name = enemy_name[0][0] if enemy_name else None
    who_moves = execute_statement_fabric(constant.SELECT_WHO_MOVES_STATEMENT)(challenge_id)[0][0]
    position = json.loads(execute_statement_fabric(constant.SELECT_POSITION)(challenge_id)[0][0])[constant.BOARD]
    history = json.loads(execute_statement_fabric(constant.SELECT_HISTORY_STATEMENT)(challenge_id)[0][0])
    last_move = json.loads(execute_statement_fabric(constant.SELECT_LAST_MOVE_STATEMENT)(challenge_id)[0][0])
    status_move = last_move[constant.STATUS_MOVE] if last_move else None
    data = {constant.POSITION: position, constant.COLOR: color, constant.WHO_MOVES: who_moves,
            constant.STATUS: True, constant.HISTORY_MOVE: history, constant.STATUS_MOVE: status_move,
            constant.DATA_FOR_THE_PAWN_QUINCE: constant.NAME_IMAGE_FOR_THE_PAWN_QUINCE[color],
            constant.MY_NAME: my_name, constant.ENEMY_NAME: enemy_name}
    return data


@_app.route(PREFIX_TO_URL+'/chess/highlight_possible_moves/<id_player>/<piece_id>')  # AJAX запит, щоб підсвітити
@safe_route                                                                          # можливі ходи
def highlight_possible_moves(id_player, piece_id):
    challenge_id, color, my_name = execute_statement_fabric(constant.SELECT_CHALLENGE_STATEMENT)(id_player)[0]
    position = json.loads(execute_statement_fabric(constant.SELECT_POSITION)(challenge_id)[0][0])
    possible_moves = chess_possible_moves.get_real_possible_moves(piece_id, position)
    moves = [dict(chain({constant.START: piece_id, constant.FINISH: coord}.items(),
                  move[coord].items())) for move in possible_moves for coord in move]
    data_for_possible_moves = {constant.DATA: [key for pieces_dict in possible_moves for key in pieces_dict] +
                                              [piece_id],
                               constant.STATUS: True, constant.POSSIBLE_MOVES: moves}
    return data_for_possible_moves


@_app.route(PREFIX_TO_URL+'/chess/move/<id_player>/<start_id>/<end_id>/<piece>')  # AJAX запит, щоб змінити позицію
@safe_route                                                                       # після свого ходу
def move(id_player, start_id, end_id, piece):
    my_move = None
    challenge_id, color, my_name = execute_statement_fabric(constant.SELECT_CHALLENGE_STATEMENT)(id_player)[0]
    position = json.loads(execute_statement_fabric(constant.SELECT_POSITION)(challenge_id)[0][0])
    possible_moves = chess_possible_moves.get_real_possible_moves(start_id, position)
    for my_move in possible_moves:
        if end_id in my_move:
            if piece != constant.JS_FALSE:
                my_move[end_id][constant.THE_PAWN_QUEENS] = [piece][0]
            break
    short_name_piece = position[constant.BOARD][start_id][constant.SHORT_NAME]
    chess_move.to_move_a_piece(start_id, my_move, position)
    this_move = my_move[end_id]
    if piece != constant.JS_FALSE:
        status_move = chess_possible_moves.return_status_move(position, True, color)
        this_move[constant.STATUS_MOVE] = status_move
        this_move[constant.NOTATION] = chess_possible_moves.move_notation(start_id, end_id, my_move, short_name_piece,
                                                                          status_move)
    this_move.update({constant.START: start_id, constant.FINISH: end_id})
    moves_of_enemy = chess_possible_moves.evaluation_position(position, constant.COLOR_FLIP_FLOP[color])
    evaluation_move = this_move[constant.STATUS_MOVE]
    who_moves = constant.COLOR_FLIP_FLOP[color]
    if not moves_of_enemy and evaluation_move:
        this_move[constant.STATUS_MOVE] = constant.MATE
        who_moves = constant.E
        this_move[constant.NOTATION] = chess_possible_moves.change_notation(this_move[constant.NOTATION], '', constant.
                                                                            STATUS_MOVE_NOTATION[constant.CHECK],
                                                                            constant.STATUS_MOVE_NOTATION[constant.
                                                                            MATE])
    elif not moves_of_enemy and not evaluation_move:
        this_move[constant.STATUS_MOVE] = constant.STALEMATE
        who_moves = constant.E
        this_move[constant.NOTATION] = chess_possible_moves.change_notation(this_move[constant.NOTATION], '', constant.
                                                                            STATUS_MOVE_NOTATION[constant.DOUBLE_CHECK],
                                                                            constant.STATUS_MOVE_NOTATION[constant.
                                                                            STALEMATE])
    last_move = json.dumps(this_move)
    position = json.dumps(position)
    history = json.loads(execute_statement_fabric(constant.SELECT_HISTORY_STATEMENT)(challenge_id)[0][0])
    history = json.dumps(history_updater(history, this_move[constant.NOTATION]))
    execute_statement_fabric(constant.UPDATE_HISTORY_STATEMENT)(history, challenge_id)
    execute_statement_fabric(constant.UPDATE_POSITION_STATEMENT)(position, who_moves, last_move, challenge_id)
    return {constant.STATUS: True, constant.STATUS_MOVE: this_move[constant.STATUS_MOVE],
            constant.NOTATION: this_move[constant.NOTATION]}


@_app.route(PREFIX_TO_URL+'/chess/action/<id_player>/<action>')  # AJAX запит при здачі
@safe_route
def surrender(id_player, action):
    status = False
    challenge_id, color, my_name = execute_statement_fabric(constant.SELECT_CHALLENGE_STATEMENT)(id_player)[0]
    if action == constant.SURRENDER_ACTION:
        execute_statement_fabric(constant.UPDATE_WHO_MOVES_STATEMENT)(constant.SURRENDER, challenge_id)
        status = True
    elif action == constant.DRAW_ACTION:

        execute_statement_fabric(constant.UPDATE_DRAW_STATEMENT)(constant.DRAW, challenge_id)
        status = True
    return {constant.STATUS: status}


@_app.route(PREFIX_TO_URL+'/chess/enemy_name/<id_player>')  # AJAX запит при запить імені супротивника
@safe_route
def give_me_name(id_player):
    challenge_id, color, my_name = execute_statement_fabric(constant.SELECT_CHALLENGE_STATEMENT)(id_player)[0]
    enemy_name = execute_statement_fabric(constant.SELECT_ENEMY_NAME_STATEMENT)(challenge_id,
                                                                                constant.COLOR_FLIP_FLOP[color])
    enemy_name = enemy_name[0][0] if enemy_name else None
    status = True if enemy_name else False
    return {constant.STATUS: status, constant.ENEMY_NAME: enemy_name}


@_app.route(PREFIX_TO_URL+'/chess/draw/<id_player>')  # AJAX запит при згоді на нічию
@safe_route
def draw(id_player):
    status = True
    challenge_id, color, my_name = execute_statement_fabric(constant.SELECT_CHALLENGE_STATEMENT)(id_player)[0]
    execute_statement_fabric(constant.UPDATE_WHO_MOVES_STATEMENT)(constant.DRAW, challenge_id)
    return {constant.STATUS: status}


@_app.route(PREFIX_TO_URL+'/chess/give_my_move/<id_player>')  # AJAX запит, щоб змінити позицію після ходу супротивника
@safe_route
def new_move(id_player):
    challenge_id, color, my_name = execute_statement_fabric(constant.SELECT_CHALLENGE_STATEMENT)(id_player)[0]
    who_moves = execute_statement_fabric(constant.SELECT_WHO_MOVES_STATEMENT)(challenge_id)[0][0]
    draw = execute_statement_fabric(constant.SELECT_DRAW_STATEMENT)(challenge_id)[0][0]
    history = json.loads(execute_statement_fabric(constant.SELECT_HISTORY_STATEMENT)(challenge_id)[0][0])
    status = True
    status_draw = None
    result_game = None
    if draw == constant.DRAW:
        status_draw = draw
    if who_moves == color or who_moves == constant.E:
        data = json.loads(execute_statement_fabric(constant.SELECT_LAST_MOVE_STATEMENT)(challenge_id)[0][0])
        data.update({constant.STATUS: status, constant.STATUS_DRAW: status_draw})
        if who_moves == constant.E:
            result = '1:0' if color == constant.COLOR_BLACK else '0:1'
            if data[constant.STATUS_MOVE] == constant.STALEMATE:
                result = '1/2:1/2'
            result_game = history_updater(history, result)
    elif who_moves == constant.SURRENDER:
        result = '0:1' if color == constant.COLOR_BLACK else '1:0'
        result_game = history_updater(history, result)
        data = {constant.STATUS: status, constant.STATUS_MOVE: who_moves, constant.STATUS_DRAW: status_draw}
    elif who_moves == constant.DRAW:
        result = '1/2:1/2'
        result_game = history_updater(history, result)
        data = {constant.STATUS: status, constant.STATUS_MOVE: who_moves}
    else:
        status = False
        data = {constant.STATUS: status}
    if status:
        execute_statement_fabric(constant.UPDATE_DRAW_STATEMENT)(constant.COLOR_WHITE, challenge_id)
    if result_game:
        history = json.dumps(result_game)
        execute_statement_fabric(constant.UPDATE_HISTORY_STATEMENT)(history, challenge_id)
    return data

if __name__ == '__main__':
    run(app=app, host='localhost', port=8080)
