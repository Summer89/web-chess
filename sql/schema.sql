CREATE TABLE invite
	(
		i_id serial primary key,
		i_side_one_uuid uuid,
		i_side_two_uuid uuid,
		i_accepted_at timestamp,
		i_challenge bigint
	);
CREATE TABLE challenge
	(
		ch_id serial primary key
	);
CREATE TYPE piece_colors AS ENUM ('w', 'b', 'e', 's', 'd');
CREATE TABLE player
	(
		p_id serial primary key,
		p_name VARCHAR(128),
		p_created_at timestamp,
		p_side  piece_colors,
		p_challenge bigint,
		p_player_uuid uuid
	);
CREATE TABLE games
	(
		g_id serial primary key,
		g_challenge bigint,
		g_position jsonb,
		g_who_moves piece_colors,
		g_last_move jsonb,
		g_history jsonb,
		g_draw piece_colors
	);
