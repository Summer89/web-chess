<!DOCTYPE html>
<html lang="en">
  <head>
      <meta http-equiv="Content-Type" content="text/html" charset="UTF-8"/>
	  <link rel="stylesheet" type="text/css" href="{{prefix}}/chess/staticfiles/css/style.css"/>
	  <title>Шахи через мережу</title>
  </head>
  <body>
    <form method='post' action='{{prefix}}/chess/' accept-charset="UTF-8">
        <div class="start_dives" id="welcome">
            <h4 id="r" >Вітаю тебе друже!</h4>
            Якщо ти уже сюди заходив і в тебе є активні партії, ти можеш вибрати їх у списку активних партій.
            Коли ти зареєстрований, то ти можеш не вказувати своє ім'я, а якщо
            ти завітав як гість, тоді давай знайомитися.<br>
            Введи своє ім'я в поле:<br><br>
            <input type="text" name="name_player" size="53" maxlength="60" value="player" onfocus="if(
            this.value=='player')this.value=''" onblur="if(this.value=='')this.value='player'"><br><br>
            Для того, щоб прийняти виклик свого опонента, введи присланий ним код запрошення
            в поле, та натисни кнопку "Прийняти виклик", і ти автоматично перейдеш до згенерованої
            ним партії.<br><br>
            <input type="text" name="two_uuid" size="53" maxlength="60" value="Введіть зсилку" onfocus="if(
            this.value)this.value=''" onblur="if(this.value=='')this.value='Введіть зсилку'"><br><br>
            <div class="button hor_cen">
                <input type="submit" name="accept" value="Прийняти виклик" class="button"><br><br>
            </div><br>
            Для того, щоб згенерувати партію, спершу вибери колір фігур, якими ти хочеш грати:
            <div id="radio_input">
                <div class="square line" id="one_box">
                    <input id="first" type="radio" name="color" value="w" checked hidden>
                    <label for="first"></label>
                </div>
                <div class="square line" id="two_box">
                    <input id="second" type="radio" name="color" value="b" hidden>
                    <label for="second"></label>
                </div>
            </div>
            Якщо ти вже вибрав колір, натисни на кнопку "Згенерувати партію", і ти перейдеш на сторінку із
            згенерованим кодом запрошення.<br><br>
            <div class="button hor_cen">
                <input type="submit" name="generate" value="Згенерувати партію" class="button">
            </div>
        </div>
    </form>
    <div class="list_games">Список активних партій
    		<div class="history scroll_yes" id="active_games">
            </div>
        <script type="text/javascript" charset="UTF-8" async>
        (function(){
            var dataForActiveGame = {{!activeGames}};
            function games(data){
                var div = document.getElementById('active_games');
                var textValue;
                for (var i = 0; i < dataForActiveGame.length; i++){
                    if (dataForActiveGame.length > 1){
                        textValue = String(i+1) + ' ' + dataForActiveGame[i][0];
                    }else{
                        textValue = dataForActiveGame[i][0]
                    }
                    var paragraph = document.createElement('br');
                    var refer = document.createElement('a');
                    var textElem = document.createTextNode(textValue);
                    var linc = dataForActiveGame[i][1];
                    if (linc){
                        refer.href = dataForActiveGame[i][1];
                    }
                    refer.className = 'games_notation';
                    refer.appendChild(textElem);
                    div.appendChild(refer);
                    div.appendChild(paragraph);
                }
            }
        games(dataForActiveGame);
        }) (window);
        </script>
        <script>
        (function(){
            function mouseOnHref(elem, condition){
 		        elem.style.color = condition ? "#000000" : "";
 	        }

	        function highlightHref(){
 		        var onmouseover = function() {mouseOnHref(this, true);};
    	        var onmouseout = function() {mouseOnHref(this, false);};
    	        var highlightingHrefs = document.getElementsByTagName("a");
    	        for (var i=0; i<highlightingHrefs.length; i++){
    	            if (highlightingHrefs[i].href){
    		            highlightingHrefs[i].onmouseover = onmouseover;
    		            highlightingHrefs[i].onmouseout = onmouseout;
    		        }
    	        }
	        }
	        highlightHref()
	    }) (window);
        </script>
		</div>
    <div id="copyright">&copy; Bezobiuk Igor 2017</div>
  </body>
</html>