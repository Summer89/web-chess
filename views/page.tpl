
<!doctype html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="UTF-8"/>
		<link rel="stylesheet" type="text/css" href="{{prefix}}/chess/staticfiles/css/style.css"/>
		<title>Шахи через мережу</title>
	</head>
	<body>
		<div class="names" id="my_name"></div>
		<div class="names" id="enemy_name"></div>
		<div class = "draw_prop_ind games_notation one_line" id = "draw_prop"></div>
		<div class="actions one_line">
		<a href="{{prefix}}/web_chess/" class="action">Вибрати іншу партію</a>
		<p class="choice_action" id="surrender">Здатися</p>
		<p class="choice_action" id="draw">Нічия?</p>
		</div>
    	<div class = "moves_history">Нотація партії
    		<div class = "history games_notation" id="history"></div>
		</div>
		<div class = "status_move games_notation one_line" id = "massage"></div>
		<div class="enemy_moved" id="anime">
			<div id="floatingCirclesG">
			<div class="f_circleG" id="frotateG_01"></div>
			<div class="f_circleG" id="frotateG_02"></div>
			<div class="f_circleG" id="frotateG_03"></div>
			<div class="f_circleG" id="frotateG_04"></div>
			<div class="f_circleG" id="frotateG_05"></div>
			<div class="f_circleG" id="frotateG_06"></div>
			<div class="f_circleG" id="frotateG_07"></div>
			<div class="f_circleG" id="frotateG_08"></div>
		</div>
			Очікуйте...
		</div>
		<div id="copyright">&copy; Bezobiuk Igor 2017</div>
		<script type='text/javascript' async>
			var playerId = '{{!meUuid}}';
			var prefix = '{{!prefix}}';
		</script>
		<script type='text/javascript' src="{{prefix}}/chess/staticfiles/script.js" async>
		</script>
	</body>
</html>