<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8"/>
    <link rel="stylesheet" type="text/css" href="{{prefix}}/chess/staticfiles/css/style.css"/>
    <title>Шахи через мережу</title>
</head>
<body>
    <div class="start_dives center" id="invite_dive">
        Скопіюйте код запрошення, та перешліть вашому опоненту по e-mail або Skype:<br><br>
        <div class=" button hor_cen" id="inv_uuid">
            <button id="cl_b" onclick="copyToClipboard(document.getElementById('cl_b').innerHTML)">{{invite}}</button>
        </div>
        <br>
        <script>
            function copyToClipboard(text) {
                window.prompt("Скопіювати в буфер: натисніть Ctrl+C, потім Enter", text);
            }
        </script>
        А також відправте посилання на цей сайт.<br>
        Після цього ви можете перейти до цієї партії, натиснувши на кнопку "Зайти в цю партію",<br>
        <br>
        <div class="button hor_cen">
            <a href="{{prefix}}/chess/game/{{meUuid}}" class='games_notation'>
            <input type="button" name="go" value="Зайти в цю партію" class="button"></a>
        </div><br>
        або перейти до стартової сторінки, щоб вибрати активну партію із списку,
        натиснувши кнопку "Вибрати іншу партію".<br><br>
        <div class="button hor_cen">
            <a href="{{prefix}}/web_chess/" class='games_notation'>
            <input type="button" name="back" value="Вибрати іншу партію" class="button"></a>
        </div>
    </div>
    <div id="copyright">&copy; Bezobiuk Igor 2017</div>
</body>
</html>