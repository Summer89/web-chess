#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = "bezobiuk"

from settings import PREFIX_TO_URL

LITERAL_NUMBER = {"A": 1, "B": 2, "C": 3, "D": 4, "E": 5, "F": 6, "G": 7, "H": 8}
COLOR_WHITE = "w"
COLOR_BLACK = "b"
SIZE_X = 8
SIZE_Y = 8
WHITE_DIRECTION = 1
BLACK_DIRECTION = -1
WHITE_Y = 2
BLACK_Y = 7
WHITE_TUNNEL_Y = 5
BLACK_TUNNEL_Y = 4
SURRENDER = "s"
DRAW = "d"
CHECK = 'check'
DOUBLE_CHECK = 'double_check'
MATE = 'mate'
STALEMATE = 'stalemate'
DRAW_ACTION = 'draw'
SURRENDER_ACTION = 'surrender'
RESULT_GAME = 'result_game'
JS_FALSE = "false"
DATA = "data"
STATUS = "status"
STATUS_MOVE = "status_move"
STATUS_DRAW = "status_draw"
LIST_CHALLENGE = "list_challenge"
POSSIBLE_MOVES = "possible_moves"
TAKE_A_PIECE = "take_a_piece"
DROP_A_PIECE = "drop_a_piece"
INSERT_A_PIECE = "insert_a_piece"
THE_PAWN_QUEENS = "the_pawn_queens"
START = "start"
FINISH = "finish"
POSITION = "position"
HISTORY_MOVE = "history_move"
COLOR = "color"
E = 'e'
COLOR_PIECE = "color_piece"
WHO_MOVES = "who_moves"
BOARD = "board"
SHORT_NAME = "short_name"
LAST_MOVED_PIECE = "last_moved_piece"
PREVIOUS_COORDINATE = "previous_coordinate"
DATA_FOR_THE_PAWN_QUINCE = "data_for_the_pawn_queens"
NOTATION = 'notation'
LONG_CASTLING = 'o-o-o'
SHORT_CASTLING = 'o-o'
SHORT_NAME_PAWN = "P"
SHORT_NAME_ROOK = "R"
SHORT_NAME_BISHOP = "B"
SHORT_NAME_KNIGHT = "N"
SHORT_NAME_QUEEN = "Q"
SHORT_NAME_KING = "K"
NAME_IMAGE = "name_image"
MY_NAME = "my_name"
ENEMY_NAME = "enemy_name"
INDEX_0 = 0
INDEX_1 = 1
INDEX_2 = 2
WHITE_PAWN_IMAGE = "P_w.png"
BLACK_PAWN_IMAGE = "P_b.png"
WHITE_ROOK_IMAGE = "R_w.png"
BLACK_ROOK_IMAGE = "R_b.png"
WHITE_KNIGHT_IMAGE = "Kn_w.png"
BLACK_KNIGHT_IMAGE = "Kn_b.png"
WHITE_BISHOP_IMAGE = "B_w.png"
BLACK_BISHOP_IMAGE = "B_b.png"
WHITE_QUEEN_IMAGE = "Q_w.png"
BLACK_QUEEN_IMAGE = "Q_b.png"
WHITE_KING_IMAGE = "K_w.png"
BLACK_KING_IMAGE = "K_b.png"
NO_ACCEPTED_GAME = "No accepted game"
NO_ACTIVE_GAMES = "Active games not found"
ACTIVE_GAMES = 'active_games'
IMPOSSIBLE_PAGE = 'impossible_page'
ERROR_PAGE = 'error_page'
START_PAGE = 'start_page'
INVITE_PAGE = 'invite_page'
INVALID_UUID_PAGE = 'invalid_uuid'
PAGE = 'page'
URL_CHESS_GAME = PREFIX_TO_URL+'/chess/game/'
URL_START_POSITION = PREFIX_TO_URL+"/chess/start_position/"
URL_LINK_RIVAL = PREFIX_TO_URL+"/chess/link/"
UPDATE_POSITION_STATEMENT = "UPDATE games SET g_position = $1::text::jsonb, g_who_moves = $2," \
                            " g_last_move = $3::text::jsonb WHERE g_challenge = $4;"
SELECT_WHO_MOVES_STATEMENT = "SELECT g_who_moves FROM games WHERE g_challenge = $1;"
SELECT_POSITION = "SELECT g_position FROM games WHERE g_challenge = $1;"
SELECT_ACTIVE_GAMES_STATEMENT = "SELECT p_name, i_accepted_at FROM player, invite WHERE invite.i_challenge = $1 AND " \
                                "player.p_challenge = $1 AND player.p_side = $2;"
SELECT_CHALLENGE_STATEMENT = "SELECT p_challenge, p_side, p_name FROM player WHERE p_player_uuid = $1;"
GET_SEQUENCE_STATEMENT = "SELECT nextval ('challenge_ch_id_seq');"
INSERT_INVITE_STATEMENT = "INSERT INTO invite (i_side_one_uuid, i_side_two_uuid, i_challenge) VALUES ($1, $2, $3);"
CREATE_PLAYER_STATEMENT = "INSERT INTO player (p_name, p_created_at, p_side, p_challenge, p_player_uuid) VALUES" \
                          " ($1, $2::text::TIMESTAMP WITHOUT TIME ZONE, $3, $4, $5);"
CREATE_GAME_STATEMENT = "INSERT INTO games (g_challenge, g_position, g_who_moves, g_history, g_last_move, g_draw" \
                        ") VALUES ($1, $2::text::jsonb, $3, $4::text::jsonb, $5::text::jsonb, $6);"
INSERT_CHALLENGE_STATEMENT = "INSERT INTO challenge VALUES ($1);"
SELECT_CHALLENGE_INVITE_STATEMENT = "SELECT i_challenge FROM invite WHERE i_side_two_uuid = $1;"
ACCEPT_INVITE_STATEMENT = "UPDATE invite SET i_accepted_at = $1::text::TIMESTAMP WITHOUT TIME ZONE" \
                          " WHERE i_challenge = $2;"
SELECT_INVITE_STATEMENT = "SELECT i_side_two_uuid FROM invite WHERE i_challenge = $1;"
SELECT_LAST_MOVE_STATEMENT = "SELECT g_last_move FROM games WHERE g_challenge = $1;"
SELECT_COLOR_ENEMY_PLAYER = "SELECT p_side FROM player WHERE p_challenge = $1;"
SELECT_HISTORY_STATEMENT = "SELECT g_history FROM games WHERE g_challenge = $1;"
UPDATE_HISTORY_STATEMENT = "UPDATE games SET g_history = $1::text::jsonb WHERE g_challenge = $2;"
UPDATE_WHO_MOVES_STATEMENT = "UPDATE games SET g_who_moves = $1 WHERE g_challenge = $2;"
SELECT_ENEMY_NAME_STATEMENT = "SELECT p_name FROM player WHERE p_challenge = $1 AND p_side = $2;"
UPDATE_DRAW_STATEMENT = "UPDATE games SET g_draw = $1 WHERE g_challenge = $2;"
SELECT_DRAW_STATEMENT = "SELECT g_draw FROM games WHERE g_challenge = $1;"
CHECKS = {INDEX_0: None, INDEX_1: CHECK, INDEX_2: DOUBLE_CHECK}
PIECE = {WHITE_ROOK_IMAGE: {COLOR_PIECE: COLOR_WHITE, SHORT_NAME: SHORT_NAME_ROOK},
         BLACK_ROOK_IMAGE: {COLOR_PIECE: COLOR_BLACK, SHORT_NAME: SHORT_NAME_ROOK},
         WHITE_KNIGHT_IMAGE: {COLOR_PIECE: COLOR_WHITE, SHORT_NAME: SHORT_NAME_KNIGHT},
         BLACK_KNIGHT_IMAGE: {COLOR_PIECE: COLOR_BLACK, SHORT_NAME: SHORT_NAME_KNIGHT},
         WHITE_BISHOP_IMAGE: {COLOR_PIECE: COLOR_WHITE, SHORT_NAME: SHORT_NAME_BISHOP},
         BLACK_BISHOP_IMAGE: {COLOR_PIECE: COLOR_BLACK, SHORT_NAME: SHORT_NAME_BISHOP},
         WHITE_QUEEN_IMAGE: {COLOR_PIECE: COLOR_WHITE, SHORT_NAME: SHORT_NAME_QUEEN},
         BLACK_QUEEN_IMAGE: {COLOR_PIECE: COLOR_BLACK, SHORT_NAME: SHORT_NAME_QUEEN}}
COLOR_FLIP_FLOP = {COLOR_WHITE: COLOR_BLACK, COLOR_BLACK: COLOR_WHITE}
NAME_IMAGE_FOR_THE_PAWN_QUINCE = {COLOR_WHITE: [WHITE_QUEEN_IMAGE, WHITE_ROOK_IMAGE,
                                                WHITE_BISHOP_IMAGE, WHITE_KNIGHT_IMAGE],
                                  COLOR_BLACK: [BLACK_QUEEN_IMAGE, BLACK_ROOK_IMAGE,
                                                BLACK_BISHOP_IMAGE, BLACK_KNIGHT_IMAGE]}
STATUS_MOVE_NOTATION = {CHECK: '+', DOUBLE_CHECK: '++', MATE: '#', STALEMATE: '='}

START_POSITION = {BOARD:
                  {"A1": {NAME_IMAGE: WHITE_ROOK_IMAGE, COLOR_PIECE: COLOR_WHITE, SHORT_NAME: SHORT_NAME_ROOK,
                          PREVIOUS_COORDINATE: None},
                   "B1": {NAME_IMAGE: WHITE_KNIGHT_IMAGE, COLOR_PIECE: COLOR_WHITE, SHORT_NAME: SHORT_NAME_KNIGHT,
                          PREVIOUS_COORDINATE: None},
                   "C1": {NAME_IMAGE: WHITE_BISHOP_IMAGE, COLOR_PIECE: COLOR_WHITE, SHORT_NAME: SHORT_NAME_BISHOP,
                          PREVIOUS_COORDINATE: None},
                   "D1": {NAME_IMAGE: WHITE_QUEEN_IMAGE, COLOR_PIECE: COLOR_WHITE, SHORT_NAME: SHORT_NAME_QUEEN,
                          PREVIOUS_COORDINATE: None},
                   "E1": {NAME_IMAGE: WHITE_KING_IMAGE, COLOR_PIECE: COLOR_WHITE, SHORT_NAME: SHORT_NAME_KING,
                          PREVIOUS_COORDINATE: None},
                   "F1": {NAME_IMAGE: WHITE_BISHOP_IMAGE, COLOR_PIECE: COLOR_WHITE, SHORT_NAME: SHORT_NAME_BISHOP,
                          PREVIOUS_COORDINATE: None},
                   "G1": {NAME_IMAGE: WHITE_KNIGHT_IMAGE, COLOR_PIECE: COLOR_WHITE, SHORT_NAME: SHORT_NAME_KNIGHT,
                          PREVIOUS_COORDINATE: None},
                   "H1": {NAME_IMAGE: WHITE_ROOK_IMAGE, COLOR_PIECE: COLOR_WHITE, SHORT_NAME: SHORT_NAME_ROOK,
                          PREVIOUS_COORDINATE: None},
                   "A2": {NAME_IMAGE: WHITE_PAWN_IMAGE, COLOR_PIECE: COLOR_WHITE, SHORT_NAME: SHORT_NAME_PAWN,
                          PREVIOUS_COORDINATE: None},
                   "B2": {NAME_IMAGE: WHITE_PAWN_IMAGE, COLOR_PIECE: COLOR_WHITE, SHORT_NAME: SHORT_NAME_PAWN,
                          PREVIOUS_COORDINATE: None},
                   "C2": {NAME_IMAGE: WHITE_PAWN_IMAGE, COLOR_PIECE: COLOR_WHITE, SHORT_NAME: SHORT_NAME_PAWN,
                          PREVIOUS_COORDINATE: None},
                   "D2": {NAME_IMAGE: WHITE_PAWN_IMAGE, COLOR_PIECE: COLOR_WHITE, SHORT_NAME: SHORT_NAME_PAWN,
                          PREVIOUS_COORDINATE: None},
                   "E2": {NAME_IMAGE: WHITE_PAWN_IMAGE, COLOR_PIECE: COLOR_WHITE, SHORT_NAME: SHORT_NAME_PAWN,
                          PREVIOUS_COORDINATE: None},
                   "F2": {NAME_IMAGE: WHITE_PAWN_IMAGE, COLOR_PIECE: COLOR_WHITE, SHORT_NAME: SHORT_NAME_PAWN,
                          PREVIOUS_COORDINATE: None},
                   "G2": {NAME_IMAGE: WHITE_PAWN_IMAGE, COLOR_PIECE: COLOR_WHITE, SHORT_NAME: SHORT_NAME_PAWN,
                          PREVIOUS_COORDINATE: None},
                   "H2": {NAME_IMAGE: WHITE_PAWN_IMAGE, COLOR_PIECE: COLOR_WHITE, SHORT_NAME: SHORT_NAME_PAWN,
                          PREVIOUS_COORDINATE: None},
                   "A8": {NAME_IMAGE: BLACK_ROOK_IMAGE, COLOR_PIECE: COLOR_BLACK, SHORT_NAME: SHORT_NAME_ROOK,
                          PREVIOUS_COORDINATE: None},
                   "B8": {NAME_IMAGE: BLACK_KNIGHT_IMAGE, COLOR_PIECE: COLOR_BLACK, SHORT_NAME: SHORT_NAME_KNIGHT,
                          PREVIOUS_COORDINATE: None},
                   "C8": {NAME_IMAGE: BLACK_BISHOP_IMAGE, COLOR_PIECE: COLOR_BLACK, SHORT_NAME: SHORT_NAME_BISHOP,
                          PREVIOUS_COORDINATE: None},
                   "D8": {NAME_IMAGE: BLACK_QUEEN_IMAGE, COLOR_PIECE: COLOR_BLACK, SHORT_NAME: SHORT_NAME_QUEEN,
                          PREVIOUS_COORDINATE: None},
                   "E8": {NAME_IMAGE: BLACK_KING_IMAGE, COLOR_PIECE: COLOR_BLACK, SHORT_NAME: SHORT_NAME_KING,
                          PREVIOUS_COORDINATE: None},
                   "F8": {NAME_IMAGE: BLACK_BISHOP_IMAGE, COLOR_PIECE: COLOR_BLACK, SHORT_NAME: SHORT_NAME_BISHOP,
                          PREVIOUS_COORDINATE: None},
                   "G8": {NAME_IMAGE: BLACK_KNIGHT_IMAGE, COLOR_PIECE: COLOR_BLACK, SHORT_NAME: SHORT_NAME_KNIGHT,
                          PREVIOUS_COORDINATE: None},
                   "H8": {NAME_IMAGE: BLACK_ROOK_IMAGE, COLOR_PIECE: COLOR_BLACK, SHORT_NAME: SHORT_NAME_ROOK,
                          PREVIOUS_COORDINATE: None},
                   "A7": {NAME_IMAGE: BLACK_PAWN_IMAGE, COLOR_PIECE: COLOR_BLACK, SHORT_NAME: SHORT_NAME_PAWN,
                          PREVIOUS_COORDINATE: None},
                   "B7": {NAME_IMAGE: BLACK_PAWN_IMAGE, COLOR_PIECE: COLOR_BLACK, SHORT_NAME: SHORT_NAME_PAWN,
                          PREVIOUS_COORDINATE: None},
                   "C7": {NAME_IMAGE: BLACK_PAWN_IMAGE, COLOR_PIECE: COLOR_BLACK, SHORT_NAME: SHORT_NAME_PAWN,
                          PREVIOUS_COORDINATE: None},
                   "D7": {NAME_IMAGE: BLACK_PAWN_IMAGE, COLOR_PIECE: COLOR_BLACK, SHORT_NAME: SHORT_NAME_PAWN,
                          PREVIOUS_COORDINATE: None},
                   "E7": {NAME_IMAGE: BLACK_PAWN_IMAGE, COLOR_PIECE: COLOR_BLACK, SHORT_NAME: SHORT_NAME_PAWN,
                          PREVIOUS_COORDINATE: None},
                   "F7": {NAME_IMAGE: BLACK_PAWN_IMAGE, COLOR_PIECE: COLOR_BLACK, SHORT_NAME: SHORT_NAME_PAWN,
                          PREVIOUS_COORDINATE: None},
                   "G7": {NAME_IMAGE: BLACK_PAWN_IMAGE, COLOR_PIECE: COLOR_BLACK, SHORT_NAME: SHORT_NAME_PAWN,
                          PREVIOUS_COORDINATE: None},
                   "H7": {NAME_IMAGE: BLACK_PAWN_IMAGE, COLOR_PIECE: COLOR_BLACK, SHORT_NAME: SHORT_NAME_PAWN,
                          PREVIOUS_COORDINATE: None}},
                  LAST_MOVED_PIECE: None}
